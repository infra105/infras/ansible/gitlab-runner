Установка gitlab-runner
=========

Описание установки gitlab-runner на сервер

Ansible
--------------

Для начала, установите ansible

- https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu

Затем скопируйте себе репозиторий:

- git clone https://gitlab.com/infra105/infras/ansible/nginx_proxy.git

IP адреса
--------------


Создать файл hosts или изменить файл в каталоге /etc/ansible/hosts. В файле нужно указать ip адрес(а) вашего(их) сервера(ов):
-  nginx_1 ansible_host=3.238.221.38 ansible_user=ubuntu ansible_ssh_private_key=~/.ssh/id_rsa
1. nginx_1 - любое имя для сервера
2. ansible_host - ip адрес сервера на котором хотите установить nginx
3. ansible_user - пользователь под которым будет производится подключение
4. ansible_ssh_private_key - сертификат, с помощью которого возможно подключение к серверу (его нужно создать зарание)

PS Здесь нужно указать ip адрес сервера, на котором будет работать сервис



Установка nginx серверов с reverse proxy
----------------

	Выберите ранер, который вы хотите и запустите его установку
	Чтобы запустить установку, выполните команду:
    ansible-playbook nginx.yml -b -vv
- -b - запуск от имени администратора
- -vv - это лог для просмотра происходящих событий
